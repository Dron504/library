<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 23.03.2016
  Time: 5:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<table>
  <tr>
    <td>ID : ${user.id}</td>
  </tr>
  <tr>
    <td>First Name :  ${user.firstName}</td>
  </tr>
  <tr>
    <td>Last Name : ${user.lastName} </td>
  </tr>
  <tr>
    <td>Login :  ${user.login}</td>
  </tr>
  <tr>
    <td colspan="2"><a href="/getAccountsForSub?subId=${subscriber.id}">Accounts</a></td>
  </tr>
</table>
</body>
</html>
