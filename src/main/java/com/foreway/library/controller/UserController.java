package com.foreway.library.controller;

import com.foreway.library.domain.User;
import com.foreway.library.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 */
@Controller
public class UserController {
    private static Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @RequestMapping("/registerUser")
   public ModelAndView registerUser(@ModelAttribute User user) {
        return new ModelAndView("registerUserPage");
    }

    @RequestMapping("/createUser")
    public String createUser (@ModelAttribute User user, Model model) {
       int id = userService.createUser(user);
        model.addAttribute("id", id);
        return "redirect:/getUser";
    }

    @RequestMapping("/getUser")
    public ModelAndView getUser(@RequestParam Integer id) {
        User user = userService.getById(id);
        if (user != null)  logger.info(user.getId());
        return new ModelAndView("userPage", "user", user);
    }


}
