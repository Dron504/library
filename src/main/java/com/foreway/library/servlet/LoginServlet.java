package com.foreway.library.servlet;

import com.foreway.library.domain.User;
import com.foreway.library.dto.AuthResponse;
import com.foreway.library.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 */
@WebServlet(name = "LoginServlet" , urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    @Autowired
    AuthService authService;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String pass = request.getParameter("password");
        AuthResponse authResponse = authService.auth(login, pass);

        if (authResponse == null) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login");
            PrintWriter out= response.getWriter();
            out.println("<font color=red>Either user name or password is wrong.</font>");
            rd.include(request, response);
        }
        String sessionId = authResponse.getUuid();
        User user = authResponse.getUser();
        Cookie[] cookies = request.getCookies();
        String sessionId1 = null;
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals("sessionId")) {
                sessionId1 = cookies[i].getValue();
                break;
            }
        }
        if (sessionId1 == null) {
            response.addCookie(new Cookie("sessionId", sessionId));
        }
        User user1 = authService.getBySessionId(sessionId);
        if (user1 == null) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login");
            rd.forward(request, response);
        }
        HttpSession session = request.getSession();
        session.setAttribute("user", user1.getFirstName());
         request.setAttribute("id", user1.getId());
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/userPage");
        rd.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
