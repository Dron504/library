package com.foreway.library.dao.impl;

import com.foreway.library.dao.BookDAO;
import com.foreway.library.dao.UserDAO;
import com.foreway.library.domain.Book;
import com.foreway.library.domain.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Repository
public class UserDAOImpl implements UserDAO {

    private static Logger logger = Logger.getLogger(UserDAOImpl.class);

    @Autowired
    DataSource dataSource;
    @Autowired
    BookDAO bookDAO;

    @Override
    public int create(final User user) {

        final String sql = "INSERT INTO library.users (first_name, last_name, role, login, password) VALUES (?, ?, ?, ?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
             jdbcTemplate.update(
                    new PreparedStatementCreator() {
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                            PreparedStatement ps =
                                    connection.prepareStatement(sql, new String[] {"id"});
                            ps.setString(1, user.getFirstName());
                            ps.setString(2, user.getLastName());
                            ps.setInt(3, user.getRole());
                            ps.setString(4, user.getLogin());
                            ps.setString(5, user.getPassword()
                            );
                            return ps;
                        }
                    },
                    keyHolder);

        } catch (Exception e) {
            logger.error(e);
        }
        return keyHolder.getKey().intValue();
    }

    @Override
    public User getByID(int id) {
        String sql = "SELECT id, first_name, last_name, role, login, password FROM library.users WHERE id = ?";
        User user = null;
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            user = jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<User>() {
                @Override
                public User mapRow(ResultSet resultSet, int i) throws SQLException {
                    User u = new User();
                    u.setId(resultSet.getInt("id"));
                    u.setFirstName(resultSet.getString("first_name"));
                    u.setLastName(resultSet.getString("last_name"));
                    u.setRole(resultSet.getInt("role"));
                    u.setLogin(resultSet.getString("login"));
                    u.setPassword(resultSet.getString("password"));
                    return u;
                }
            });
        } catch (Exception e) {
            logger.error(e);
        }
        return user;

    }

    @Override
    public void update(User user) {

    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        String sql = "SELECT id, first_name, last_name, role, login, password FROM library.users";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            List<Map<String, Object>> userRows = jdbcTemplate.queryForList(sql);
            for (Map<String, Object> userRow : userRows) {
                User user = new User();
                user.setId(Integer.parseInt(String.valueOf(userRow.get("id"))));
                user.setFirstName(String.valueOf(userRow.get("first_name")));
                user.setLastName(String.valueOf(userRow.get("last_name")));
                user.setLogin(String.valueOf(userRow.get("login")));
                user.setRole(Integer.parseInt(String.valueOf(userRow.get("role"))));
                users.add(user);

            }
        } catch (Exception e) {
            logger.error(e);
        }
        return users;

    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM library.users WHERE id = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            jdbcTemplate.update(sql, id);
        } catch (Exception e) {
            logger.error(e);
        }

    }

    @Override
    public void addFavoriteBook(int userId, int bookId) {
        String sql = "INSERT INTO library.user_favorites (user_id, book_id) VALUES (?, ?)";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            jdbcTemplate.update(sql, userId, bookId);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Override
    public void deleteFavoriteBook(int userId, int bookId) {
        String sql = "DELETE FROM library.user_favorites WHERE user_id = ? AND book_id = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            jdbcTemplate.update(sql, userId,bookId);
        } catch (Exception e) {
            logger.error(e);
        }

    }

    @Override
    public List<Book> getFavorites(int userId) {
        String sql = "SELECT book_id FROM library.user_favorites WHERE user_id = ?";
        List<Book> favorites = new ArrayList<>();
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            List<Map<String, Object>> favRows = jdbcTemplate.queryForList(sql, userId);
            for (Map<String, Object> favRow : favRows) {
                favorites.add(bookDAO.getById(Integer.parseInt(String.valueOf(favRow.get("book_id")))));
            }

        } catch (Exception e) {
            logger.error(e);
        }
            return favorites;
    }

    @Override
    public Integer checkUserCredentials(String login, String password) {
        String sql = "SELECT id FROM library.users WHERE login = ? AND password = ?";
        Integer userId = null;
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
          userId = jdbcTemplate.queryForObject(sql, new Object[]{login, password}, Integer.class);
        }  catch (Exception e){
            logger.error(e);
        }
        return userId;
    }
}
