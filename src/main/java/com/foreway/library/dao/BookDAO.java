package com.foreway.library.dao;

import com.foreway.library.domain.Book;

/**
 *
 */
public interface BookDAO {

    Book getById(int id);
}
