package com.foreway.library.dao;

import com.foreway.library.domain.Book;
import com.foreway.library.domain.User;

import java.util.List;

/**
 *
 */
public interface UserDAO {

    int create(User user);

    User getByID(int id);

    void update(User user);

    List<User> getAll();

    void delete(int id);

    void addFavoriteBook(int userId, int bookId);

    void deleteFavoriteBook(int userId, int bookId);

    List<Book> getFavorites(int userId);

    Integer checkUserCredentials(String login, String password);


}
