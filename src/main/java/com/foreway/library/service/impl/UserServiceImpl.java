package com.foreway.library.service.impl;

import com.foreway.library.dao.UserDAO;
import com.foreway.library.domain.User;
import com.foreway.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDAO userDAO;

    @Override
    public int createUser(User user) {
       return userDAO.create(user);
    }

    @Override
    public void deleteUser(int id) {

    }

    @Override
    public User getById(int id) {
        return userDAO.getByID(id);
    }
}
