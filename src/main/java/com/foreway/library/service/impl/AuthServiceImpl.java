package com.foreway.library.service.impl;

import com.foreway.library.dao.UserDAO;
import com.foreway.library.domain.User;
import com.foreway.library.dto.AuthResponse;
import com.foreway.library.service.AuthService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;


/**
 *
 */
public class AuthServiceImpl implements AuthService{

    private static Logger logger = Logger.getLogger(AuthServiceImpl.class);
    private Map<String, User> userHolder = new ConcurrentHashMap<>();

    @Autowired
    private UserDAO userDAO;

    public AuthResponse auth(String login, String password) {
        AuthResponse authResponse = new AuthResponse();
        try {
            Integer subId = userDAO.checkUserCredentials(login, password);
            if (subId == null) return null;

            User user = userDAO.getByID(subId);
            String uuid = UUID.randomUUID().toString();
            userHolder.put(uuid, user);
            authResponse.setUser(user);
            authResponse.setUuid(uuid);
        } catch (Exception e) {
            logger.error(e);
        }
        return authResponse;
    }
    @Override
    public User getBySessionId(String uuid) {
        return userHolder.get(uuid);
    }

    @Override
    public void delBySessionId(String uuid) {
        userHolder.remove(uuid);
    }


}
