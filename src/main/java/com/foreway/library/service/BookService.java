package com.foreway.library.service;

import com.foreway.library.domain.Book;

import java.util.List;

/**
 *
 */
public interface BookService {
    void addBook(Book book);

    void deleteBook(int id);

    Book getById(int id);

    List<Book> getAllBooks();

    List<Book> getByName(String name);

    List<Book> getByAuthor(String author);

    List<Book> getByGenre(String genre);


}
