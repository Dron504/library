package com.foreway.library.service;

import com.foreway.library.domain.User;
import com.foreway.library.dto.AuthResponse;

/**
 *
 */
public interface AuthService {
    AuthResponse auth(String login, String password);

    User getBySessionId(String uuid);

    void delBySessionId(String uuid);

}
