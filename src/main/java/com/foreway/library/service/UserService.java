package com.foreway.library.service;

import com.foreway.library.domain.User;

/**
 *
 */
public interface UserService {

    int createUser(User user);

    void deleteUser(int id);

    User getById(int id);


}
