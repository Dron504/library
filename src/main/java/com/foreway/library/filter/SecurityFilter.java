package com.foreway.library.filter;

import com.foreway.library.domain.User;
import com.foreway.library.service.AuthService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@WebFilter(filterName = "SecurityFilter")
public class SecurityFilter implements Filter {
    private static final Set<String> ADMIN_RESOURCES = new HashSet<>();
    private static final Set<String> USER_RESOURCES = new HashSet<>();
    private static final Set<String> GUEST_RESOURCES = new HashSet<>();
    private static Logger logger = Logger.getLogger(SecurityFilter.class);
    @Autowired
    private AuthService authService;

    static {

        GUEST_RESOURCES.add("/login");
        GUEST_RESOURCES.add("");
        ADMIN_RESOURCES.add("/getUnpaidAccountsBySub");
        USER_RESOURCES.add("/getSub");

    }

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        String queryString = ((HttpServletRequest) req).getRequestURI();
        logger.info("User try to request:" + queryString);
        //  if url contents guest resources - skip
        for (String resource : GUEST_RESOURCES) {
            if (queryString.startsWith(resource)) chain.doFilter(req, resp);
        }
        Cookie[] cookies = ((HttpServletRequest) req).getCookies();
        String sessionId = null;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("sessionId")) {
                    sessionId = cookies[i].getValue();
                    break;
                }
            }
        }
        if (sessionId == null) {
            ((HttpServletResponse) resp).sendError(403);
            chain.doFilter(req, resp);
        }
        User user = authService.getBySessionId(sessionId);
        if (user == null) {
            ((HttpServletResponse) resp).sendError(403);
            chain.doFilter(req, resp);
        }
        for (String resource : USER_RESOURCES) {
            if (queryString.startsWith(resource)) chain.doFilter(req, resp);
        }
        if (user.getRole() == 1) {
            for (String resource : ADMIN_RESOURCES) {
                if (queryString.startsWith(resource)) chain.doFilter(req, resp);
            }
        }
        ((HttpServletResponse) resp).sendError(403);
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
