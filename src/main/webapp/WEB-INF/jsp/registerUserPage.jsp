<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 23.03.2016
  Time: 3:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>

</head>
<body>
<div>
    <form:form method="post" action="/createUser" modelAttribute="user">
        <table>
            <tr>
                <td>First Name :</td>
                <td><form:input path= "firstName" /></td>
            </tr>
            <tr>
                <td>Last Name :</td>
                <td><form:input path="lastName" /></td>
            </tr>
            <tr>
                <td>Login :</td>
                <td><form:input path="login" /></td>
            </tr>
            <tr>
                <td>Password :</td>
                <td><form:input path="password" /></td>
            </tr>
            <tr>
                <td> </td>
                <td><input type="submit" value="Save" /></td>
            </tr>

        </table>
    </form:form>
</div>
</body>
</html>
